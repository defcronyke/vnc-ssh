# VNC SSH  
  
Connect to a VNC server with TigerVNC vncviewer using SSH  
port forwarding. This has only been tested with the VNC  
server which comes on Raspberry Pi OS (RealVNC server free 
version) so far.  
  
### Usage:  
  
```shell
bash <(curl -sL https://tinyurl.com/vnc-ssh-forward) . <vnc-server-ip-address-for-ssh>
```  
  
(Don't forget to put the '.' in the above command.)  
  
### Optional configuration environment variables:  
  
```shell
PORT=5901		# (vnc server listen port on remote server)
NET_IFACE="lo"		# (local network interface to forward the remote vnc server to)
MY_IP="127.0.0.1"	# (ip address of $NET_IFACE)                                          
MY_PORT=5901		# (local port to forward the remote vnc server to)
DISPLAY_NUM=1		# (last digit of $MY_PORT, X session number to use for remote vnc server)
VNC_VIEWER_CONF_TMPL="localhost.tigervnc.tmpl"	# (TigerVNC vncviewer config file template to use)
VNC_VIEWER_CONF="${HOME}/localhost.tigervnc"	# (TigerVNC vncviewer config file will be output here)
```  

