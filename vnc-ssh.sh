#!/bin/bash
# Copyright (c) 2021 Jeremy Carter <jeremy@jeremycarter.ca>
#
# See the LICENSE file in the top-level folder of this
# project for the terms of use, modification and distribution
# of this project. If you don't agree to follow those terms,
# you aren't allowed to use, modify, or distribute any part
# of this project.
#
# ----------
#
# Connect to a VNC server with TigerVNC vncviewer using SSH 
# port forwarding. This has only been tested with the VNC  
# server which comes on Raspberry Pi OS (RealVNC server free 
# version) so far.  
# 
# Usage:
#
#   bash <(curl -sL https://tinyurl.com/vnc-ssh-forward) . <vnc-server-ip-address-for-ssh>
#
# (Don't forget to put the '.' in the above command.)
#
# Optional configuration environment variables:
#
#   PORT=5901		# (vnc server listen port on remote server)
#   NET_IFACE="lo"	# (local network interface to forward the remote vnc server to)
#   MY_IP="127.0.0.1"	# (ip address of $NET_IFACE)                                          
#   MY_PORT=5901	# (local port to forward the remote vnc server to)
#   DISPLAY_NUM=1	# (last digit of $MY_PORT, X session number to use for remote vnc server)
#   VNC_VIEWER_CONF_TMPL="localhost.tigervnc.tmpl"	# (TigerVNC vncviewer config file template to use)
#   VNC_VIEWER_CONF="${HOME}/localhost.tigervnc"	# (TigerVNC vncviewer config file will be output here)
#
# ----------

# Borrowed and slightly modified from:
# https://stackoverflow.com/a/246128
get_self_dir() {
	if [ $# -ge 1 ] && [ ! -z $1 ]; then
		echo "$1"
		return 0
	fi

	SOURCE="${BASH_SOURCE[0]}"

	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
		DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
		SOURCE="$(readlink "$SOURCE")"

		# If $SOURCE was a relative symlink, we need to resolve it 
		# relative to the path where the symlink file was located.
		[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
	done

	DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

	echo "$DIR"
}

# The main function.
vnc_ssh() {
	PORT=${PORT:-5901}
	NET_IFACE=${NET_IFACE:-"lo"}
	MY_IP=$(ip a ls ${NET_IFACE} | grep "inet " | xargs | cut -d' ' -f2 | cut -d'/' -f1)
	MY_PORT=${MY_PORT:-$PORT}

	display_num_str=$(echo $PORT | rev)
	DISPLAY_NUM=${DISPLAY_NUM:-${display_num_str:0:1}}

	fixed_dir=""

	if [ $# -ge 2 ]; then
		fixed_dir="$1"
		shift
	fi

	VNC_VIEWER_CONF_TMPL=${VNC_VIEWER_CONF_TMPL:-"$(get_self_dir "$fixed_dir")/localhost.tigervnc.tmpl"}
	VNC_VIEWER_CONF_TMPL_DEFAULT_URL="https://gitlab.com/defcronyke/vnc-ssh/-/raw/master/localhost.tigervnc.tmpl"

	if [ ! -f "$VNC_VIEWER_CONF_TMPL" ]; then
		echo "TigerVNC vncviewer config file template not found. Retrieving default template file: $VNC_VIEWER_CONF_TMPL_DEFAULT_URL -> $VNC_VIEWER_CONF_TMPL"
		echo ""

		curl -sL https://gitlab.com/defcronyke/vnc-ssh/-/raw/master/localhost.tigervnc.tmpl | tee "$VNC_VIEWER_CONF_TMPL"
	fi

	VNC_VIEWER_CONF=${VNC_VIEWER_CONF:-${HOME}/$(basename "${VNC_VIEWER_CONF_TMPL}" ".$(echo "$VNC_VIEWER_CONF_TMPL" | rev | cut -d'.' -f1 | rev)")}

	echo "PORT=\"$PORT\""
	echo "MY_IP=\"$MY_IP\""
	echo "MY_PORT=\"$MY_PORT\""
	echo "DISPLAY_NUM=\"$DISPLAY_NUM\""

	cat "$VNC_VIEWER_CONF_TMPL" | sed "s@{MY_IP}@$MY_IP@g" | sed "s@{MY_PORT}@$MY_PORT@g" | tee "$VNC_VIEWER_CONF"

	ssh "$1" "vncserver -localhost :$DISPLAY_NUM"
	ssh -MS vnc-socket -fnNTL $MY_IP:$MY_PORT:localhost:$PORT "$1"

	vncviewer "$VNC_VIEWER_CONF"

	ssh -S vnc-socket -O check "$1"
	ssh -S vnc-socket -O exit "$1"
}

vnc_ssh $@

